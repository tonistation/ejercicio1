/*
MySQL Backup
Source Server Version: 5.7.14
Source Database: pruebageopagos
Date: 20/9/2017 10:25:10
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
--  Table structure for `favorito`
-- ----------------------------
DROP TABLE IF EXISTS `favorito`;
CREATE TABLE `favorito` (
  `usuario_codigo` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_codigo_favorito` int(11) NOT NULL,
  PRIMARY KEY (`usuario_codigo`,`usuario_codigo_favorito`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pago`
-- ----------------------------
DROP TABLE IF EXISTS `pago`;
CREATE TABLE `pago` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `importe` double NOT NULL,
  `fecha` date DEFAULT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `usuario`
-- ----------------------------
DROP TABLE IF EXISTS `usuario`;
CREATE TABLE `usuario` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(50) NOT NULL,
  `clave` varchar(150) DEFAULT NULL,
  `edad` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `usuario_pago`
-- ----------------------------
DROP TABLE IF EXISTS `usuario_pago`;
CREATE TABLE `usuario_pago` (
  `usuario_codigo` int(11) NOT NULL,
  `pago_codigo` int(11) NOT NULL,
  PRIMARY KEY (`usuario_codigo`,`pago_codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  View definition for `view_favorito`
-- ----------------------------
DROP VIEW IF EXISTS `view_favorito`;
CREATE ALGORITHM=UNDEFINED DEFINER=`contacto`@`%` SQL SECURITY DEFINER VIEW `view_favorito` AS select `u`.`codigo` AS `codigoUsuario`,`u`.`usuario` AS `usuario`,`u2`.`codigo` AS `codigoFavorito`,`u2`.`usuario` AS `usuarioFavorito` from ((`usuario` `u` join `favorito` `f` on((`u`.`codigo` = `f`.`usuario_codigo`))) join `usuario` `u2` on((`f`.`usuario_codigo_favorito` = `u2`.`codigo`)));

-- ----------------------------
--  View definition for `view_pagos`
-- ----------------------------
DROP VIEW IF EXISTS `view_pagos`;
CREATE ALGORITHM=UNDEFINED DEFINER=`contacto`@`%` SQL SECURITY DEFINER VIEW `view_pagos` AS select `up`.`usuario_codigo` AS `codigoUsuario`,`u`.`usuario` AS `usuario`,`up`.`pago_codigo` AS `codigoPago`,`p`.`importe` AS `importe`,date_format(`p`.`fecha`,'%d-%m-%Y') AS `fecha` from ((`usuario_pago` `up` join `usuario` `u` on((`up`.`usuario_codigo` = `u`.`codigo`))) join `pago` `p` on((`up`.`pago_codigo` = `p`.`codigo`)));

-- ----------------------------
--  Records 
-- ----------------------------
INSERT INTO `favorito` VALUES ('9','15'), ('18','13');
INSERT INTO `pago` VALUES ('3','199','2017-10-19'), ('4','145','1970-01-01'), ('26','452','2017-11-15'), ('27','25','2109-01-22');
INSERT INTO `usuario` VALUES ('9','user9','5b1898798fedb3d3037f78f2779bf276','18'), ('13','fiorellakar','5b1898798fedb3d3037f78f2779bf276','25'), ('15','firektl2s','78a842189c1529901c70187c17a5d23b','29'), ('16','karinatorres','aaf0f8ba5fb9e1827d9077aef89c8c07','22'), ('18','sasdaswwwww','3494317c97eb0f6fdc2e5db2ebe5c2f5','22');
INSERT INTO `usuario_pago` VALUES ('9','3'), ('9','26'), ('14','4');
