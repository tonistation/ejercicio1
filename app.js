var app =  angular.module('frontGeoPagos', ['ngMaterial', 'ngAnimate', 'ngRoute'])
                  .constant('API', "/ejercicio1/php/api/?a=");

app.run(function($rootScope, $http, API, $location, funciones) {
    //$http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded;charset=utf-8;"; 

    $rootScope.titulo = "Ejercicio 1 - GeoPagos"; 
    $rootScope.cargaActiva = false; 
    $rootScope.baseUrl = $location.protocol() + "://" + $location.host() + "/ejercicio1/front/";   

    $rootScope.irA = function(ruta){
        window.open(ruta, '_blank');
    }  
});
 
app.config(['$mdThemingProvider', '$mdIconProvider' , '$routeProvider', '$locationProvider', '$httpProvider', function($mdtp, $mdicon, $route, $lp, $httpProvider) {   
      
    $mdtp.theme('default').primaryPalette('blue').accentPalette('lime').warnPalette('grey');
    $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
   
    //$lp.html5Mode({  enabled: true,   requireBase: true   }); 
    $mdicon.fontSet('mi', 'material-icons'); 

    $route.when('/', { 
    	templateUrl: 'app/pages/home.html',  
    	controller: homeCtrl 
    }).when('/usuarios', { 
        templateUrl: 'app/pages/usuario.html',  
        controller: usuarioCtrl
    }).when('/pagos', { 
        templateUrl: 'app/pages/pago.html',  
        controller: pagoCtrl
    }).when('/info', { 
        templateUrl: 'app/pages/info.html',  
        controller: infoCtrl
    }).when('/404', { 
    	templateUrl: 'app/pages/404.php',  
    	controller: notFound
    }).otherwise({ redirectTo:'/404' });; 
     
}]);   
 