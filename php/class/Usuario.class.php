<?php

/* Creada por jose rivas @tonistation */
/* 17-09-2017 */

/*clase para manejar usuarios*/

 
//incluir archivo clase Comun
include_once('Comun.class.php'); 
//incluir archivo clase Favorito
include_once('Favorito.class.php'); 
//incluir archivo clase Favorito
include_once('Pago.class.php');

class Usuario extends Comun
{   
    private $msjErrorFormatoUsuario;
    private $msjErrorFormatoClave;
    private $msjErrorFormatoEdad;
  
    private $favorito;
    private $pago;

    private $codigo;  
    private $usuario;
    private $clave;
    private $edad;

    /* recibe como parametros  los dato del usuario
    $bd objeto de conexion a la bd
    $usuario nick del usuario OPCIONAL
    $clave clave del usuario OPCIONAL
    $edad del usuario OPCIONAL
    */
    public function __construct($bd, $usuario = "", $clave = "", $edad = "")
	{ 
        $this->bd = $bd;   

        $this->usuario = $usuario;
        $this->clave = $clave;
        $this->edad = $edad;
        $this->estableceValoresPredU(); 
    }
    
    //establece variables que se usan en los mensajes de error
    private function estableceValoresPredU(){ 
        $this->respuesta = 'json'; 
        $this->msjErrorFormatoUsuario = ' Usuario invalido (Formato: 6-15 caracteres alfanumericos sin espacios)';
        $this->msjErrorFormatoClave = ' Clave invalida (Formato: 8-15 caracteres, debe contener al menos un numero y una lentra)';
        $this->msjErrorFormatoEdad = ' Edad invalida (Formato: valor numerico, debe ser mayor a 18 años)'; 
    }

    /*
    retorna todos los usuarios actuales
    $enTexto opcional , especifica si retorna la data en texto, si esta en false retorna en arreglo
    $filtro opcional para lista, se recibe array par valor para condicion
    salida arreglo con data 
    */
	public function lista($enTexto = true, $filtro = array())
	{ 
         return $this->listaRegistros('usuario', $enTexto, $filtro);
    }

     /*
    retorna todos los favoritos 
    $enTexto opcional , especifica si retorna la data en texto, si esta en false retorna en arreglo
    salida arreglo con data 
    */
	public function favoritos($texto = TRUE)
	{  
        if( $this->verificaCodigo('usuario', 'codigo', $this->codigo) )
        { 
            return $this->favorito->getFavoritos($texto);
        }
        else
        {
            return $this->retorno(TRUE, 'ERROR, codigo de usuario no existe en la base de datos');
        }  
    }

     /*
    retorna todos los seguidores 
    $enTexto opcional , especifica si retorna la data en texto, si esta en false retorna en arreglo
    salida arreglo con data 
    */
	public function seguidores($texto = TRUE)
	{  
        if( $this->verificaCodigo('usuario', 'codigo', $this->codigo) )
        {
            return $this->favorito->getSeguidores($texto);
        }
        else
        {
            return $this->retorno(TRUE, 'ERROR, codigo de usuario no existe en la base de datos');
        }
        
    }


      /*
    retorna todos los pagos del usuario
    $enTexto opcional , especifica si retorna la data en texto, si esta en false retorna en arreglo
    salida arreglo con data 
    */
	public function pagos($texto = TRUE)
	{ 
          return $this->pago->getPagosUsuarioActual(FALSE); 
    }

    /*
    crea usuario 
    retorna mensaje de resultado  
    */
	public function crear()
	{ 
        $msjError = '';
        $msjFinal = '';
        $error = FALSE;

        if( ! $this->validaUsuario($this->usuario) )
        {
            $msjError.= $this->msjErrorFormatoUsuario; 
            $error = TRUE;
        }

        if( ! $this->validaClave($this->clave) )
        {
            $msjError.= $this->msjErrorFormatoClave;
            $error = TRUE;
        }

        if( ! $this->validaEdad($this->edad) )
        {
            $msjError.= $this->msjErrorFormatoEdad;
            $error = TRUE;
        }

        if($error)
        {
            $msjFinal = 'Errores encontrados, NO se pudo crear el usuario.' . $msjError . ' Valores proporcionados Usuario: ' . $this->usuario . ' || Clave: ' . $this->clave . ' || Edad: ' . $this->edad;
            return $this->retorno(TRUE, $msjFinal);
        }
        else
        {
            if($this->bd->checkValue('usuario', 'usuario', $this->usuario))
            {
                $msjFinal = 'ERROR, usuario ' . $this->usuario . ' ya existe en la bd'; 
                return $this->retorno(TRUE, $msjFinal);
            }
            else
            {
                $data = array('usuario'=>$this->usuario,  'clave'=> MD5($this->clave) , 'edad'=> $this->edad ) ;
                $insert = $this->bd->insert('usuario', $data);
                if($insert){ 
                    $this->setCodigo( $this->bd->ultimoCodigo('usuario') ); 
                    $msjFinal = 'Usuario creado exitosamente'; 
                    return $this->retorno(FALSE, $msjFinal);
                }
            } 
            
        }  
    }

 

    /*
    Funcion para actualizar informacion de usuario
    $usuario nick del usuario
    $clave clave del usuario
    $edad edad del usuario
    */

    public function actualizar()
    { 

        if( $this->verificaCodigo('usuario', 'codigo', $this->codigo) )
        {
            $msjFinal = '';
            $error = FALSE;
            $cambiaClave = TRUE;
            if(!$this->validaUsuario($this->usuario))
            {   
                $error = TRUE;
                $msjFinal = $this->msjErrorFormatoUsuario;
            }
            if(!$this->clave == NULL)
            {
                if( ! $this->validaClave($this->clave) )
                {
                    $msjError.= $this->msjErrorFormatoClave;
                    $error = TRUE;
                }
            }
            else
            {
                $cambiaClave = FALSE;
            } 
            if(!$this->validaEdad($this->edad))
            {   
                $error = TRUE;
                $msjFinal = $this->msjErrorFormatoEdad;
            }
    
            if(!$error)
            { 
                $campos =  array('usuario'=> $this->usuario, 'clave'=> MD5($this->clave), 'edad'=>$this->edad );
                if(!$cambiaClave)
                {
                    $campos =  array('usuario'=> $this->usuario, 'edad'=>$this->edad );
                }
                $condicion =  array('codigo'=> $this->codigo );  
                return $this->ejecutaUpdate('usuario', $campos, $condicion, 'Data Usuario');
            }
            else
            {
                return $this->retorno(TRUE, $msjFinal);
            }  
        }
        else
        {
            return $this->retorno(TRUE, 'ERROR, codigo de usuario no existe en la base de datos');
        }
    }


    /*
    Eliminar el usuario instanciado o el definido en el parametro 
    retorna msj de proceso
    */
    public function eliminar(){  
        if( $this->verificaCodigo('usuario', 'codigo', $this->codigo) )
        {
            if( $this->verificaCodigo('usuario_pago', 'usuario_codigo', $this->codigo) )
            {
                 return $this->retorno(TRUE, 'ERROR, este usuario tiene pagos asociados.');
            }
            else{
                if($this->favorito->delete($this->codigo,0,TRUE) )
                {
                    $condicion =  array('codigo'=> $this->codigo );  
                    return $this->ejecutaDelete('usuario', $condicion, ' Usuario', FALSE); 
                }
            }
        }
        else
        {
            return $this->retorno(TRUE, 'ERROR, codigo de usuario no existe en la base de datos');
        }
    }
 

    /*
    Eliminar el usuario instanciado o el definido en el parametro 
    retorna msj de proceso
    */
    public function eliminarFull(){  
        if( $this->verificaCodigo('usuario', 'codigo', $this->codigo) )
        {
            $condicion =  array('codigo'=> $this->codigo );   
            if($this->favorito->delete($this->codigo,0,TRUE) )
            {
                 $this->pago->deletePorUsuario($this->codigo);
                 $this->ejecutaDelete('usuario', $condicion, ' Usuario');
                 return $this->retorno(FALSE, 'ERROR, Usuario eliminado exitosamente');
            }
            else
            {
                return $this->retorno(TRUE, 'ERROR, en el proceso eliminando las vinculaciondes de los favoritos'); 
            }
        }
        else
        {
            return $this->retorno(TRUE, 'ERROR, codigo de usuario no existe en la base de datos');
        }
    }
        
         
    /*
    Funcion para crear favorito a usuario instanciado o pasado por parametro
    $codigoFavorito, codigo del usuario al que se le fija el favorito 
    retorna msj de proceso
    */
    public function nuevoFavorito($codigoFavorito)
    {  
        return $this->favorito->crear($codigoFavorito); 
    }


    /*
    Funcion para eliminar favorito a usuario 
    $codigoFavorito, codigo del usuario al que se le quitara de los favorito
    retorna msj de proceso
    */
    public function eliminarFavorito($codigoFavorito){ 
        return $this->favorito->delete($codigoFavorito, FALSE);  
    } 


     /*
    Funcion para eliminar todos los favoritos del usuario 
    retorna msj de proceso
    */
    public function eliminarFavoritoTodos(){    
        return $this->favorito->delete(); 
    } 

    /*
    informacion del usuario establecido en la instancia
    retorna informacion de usuario o msj de error si no hay definido
    */
	public function getUsuarioActual()
	{   
        $data = '';
        if( $this->verificaCodigo('usuario', 'codigo', $this->codigo) )
        {
            return $this->lista(FALSE, array("codigo"=> $this->codigo));  
        }
        else
        {
            return $this->retorno(TRUE, 'ERROR, codigo de usuario no existe en la base de datos');
        } 
    }
   
    /*
    set campo codigoUsuario
    $codigo valor a modificar 
    */
	public function setCodigo( $codigo )
	{   
        $this->codigo = $codigo;
        $this->pago = new Pago($this->bd, $this->codigo );
        $this->favorito = new Favorito($this->bd, $this->codigo );
    }

     /*
    set campo usuario
    $usuario valor a modificar 
    */
	public function setUsuario( $usuario )
	{  
         $this->usuario = $usuario;
    }

      /*
    set campo clave
    $clave valor a asignar 
    */
	public function setClave( $clave )
	{  
         $this->clave = $clave;
    }

      /*
    set campo edad
    $edad valor a asignar 
    */
	public function setEdad( $edad )
	{  
         $this->edad = $edad;
    }

    /*
    get campo codigo  
    */
	public function getCodigo( )
	{  
         return $this->codigo;
    }

    /*
    get campo usuario  
    */
	public function getUsuario( )
	{  
         return $this->usuario;
    }

    /*
    get campo clave  
    */
	public function getClave( )
	{  
         return $this->clave;
    }

    /*
    get campo edad  
    */
	public function getEdad( )
	{  
         return $this->edad;
    }
     
}

 

//EJEMPLOS
//$bd = new ConnMysql( BD_SERVER, BD_USER, BD_PASS, BD_NAME );  
//$usuario = new Usuario($bd); 

//$usuario->setCodigo(9); asigna codigo de usuario para trabajar
//$usuario->setUsuario("fabiana");
//$usuario->setClave("2313213213212ss");
//$usuario->setEdad(35);
//echo $usuario->crear(); //crear usuario , retorna resultado

//$usuario->respuesta='json'; //tipo de respuesta en las funciones de ejecucion 'json', 'arrelgo' o 'texto' que es la predeterminada se imprime sin retornar
// $usuario->eliminar(); // eliminar usuario
//$usuario->lista(); lista todos los usuarios

//echo $usuario->favoritos(FALSE); //favoritos del usuario
//echo $usuario->seguidores(FALSE); //seguidores del usuario (otros usuarios los  tienen en favoritos)
//echo $usuario->pagos(FALSE); //pagos del usuario

//listar
//$usuario->nuevoFavorito(15); //asignando favorito usuario 15 al usuario , retorna resultado

//se puede instanciar sin usuario en particular
//$usuario = new Usuario(8); // o coun un usuario definido por el codigo

//echo $usuario->eliminarFavorito(15); //elimina el favorito indicado devuelde resultado de operacion

//echo $usuario->getUsuarioActual(); //obtener info del usuario actual     
   
//echo $usuario->lista(FALSE, array("codigo"=> 4)); //listando con filtro 

/* Manejo de FAVORITOS */
//$usuario->nuevoFavorito(6); //asignando favorito 8 al usuario
//$usuario->deleteFavorito(7); //asignando  favorito 7 al usuario 
//echo $usuario->eliminarFavoritoTodos(); //sin parametros elimina todos los favoritos del usuario retorna resultado

 
?>