<?php

/* Creada por jose rivas @tonistation */
/* 17-09-2017 */

/*clase para manejar usuarios*/

 
//incluir archivo clase Proceso
include_once('Comun.class.php'); 


class Favorito extends Comun
{  
     
    private $msjErrorFormatoCodigo;
    private $msjErrorCodigoInvalido; 

    protected $sqlJoinFavoritos;
    private $codigoUsuario;

    public function __construct($bd, $codigoUsuario = 0)
	{ 
        $this->bd = $bd;  
        $this->estableceVariablesFavorito();
        $this->codigoUsuario = $codigoUsuario;
        $this->respuesta = 'json';
        $this->sqlJoinFavoritos = 'select `u`.`codigo` AS `codigoUsuario`,`u`.`usuario` AS `usuario`,`u2`.`codigo` AS `codigoFavorito`,`u2`.`usuario` AS `usuarioFavorito` 
        from ((`usuario` `u` join `favorito` `f` on((`u`.`codigo` = `f`.`usuario_codigo`)))
        join `usuario` `u2` on((`f`.`usuario_codigo_favorito` = `u2`.`codigo`)))'; 
    } 

    //establece variables que se usan en los mensajes de error
    private function estableceVariablesFavorito(){
        $this->msjErrorFormatoCodigo = 'ERROR, codigo invalido, debe ser numerico';
        $this->msjErrorCodigoInvalido = 'ERROR, codigo invalido, no existe usuario.';
        $this->msjErrorEjecucion = 'ERROR de ejecucion, consulte log.';
        $this->msjSuccesEjecucion = 'Proceso realizado con exito.';  
    }

    /*
    retorna todos los registros de la taba favorito actuales
    $enTexto opcional , especifica si retorna la data en texto, si esta en false retorna en arreglo
    $filtro opcional para lista, se recibe array par valor para condicion
    salida arreglo con data 
    */
	public function lista($enTexto = true, $filtro = array())
	{ 
         return $this->listaRegistros('favorito', $enTexto, $filtro);
    }

    /*
    crea registro en tabla favorito
    $codigoUsuario codigo de usuario
    $codigoFavorito codigo del usuario a seguir
    retorna mensaje de resultado  
    */
	public function crear($codigoFavorito)
	{ 
        $msjError = '';
        $msjFinal = '';
        $error = FALSE;
 

        if( $this->codigoUsuario == $codigoFavorito)
        {
            $msjError.= 'Usuarios iguales, un usuario no puede tenerse a si mismo como favorito';
            $error = TRUE;    
        }

        $validaBD = $this->validaNuevoFavoritoBD($this->codigoUsuario, $codigoFavorito);
        if( is_numeric($validaBD) )
        {
            $msjError.= 'Cod usuario: ' . $validaBD . ' No se encuentra registrado';
            $error = TRUE;
        }

        if(!is_numeric($this->codigoUsuario) )
        {
            $msjError.= 'Cod usuario: ' .  $this->msjErrorFormatoCodigo; 
            $error = TRUE;
        }

        if(!is_numeric($codigoFavorito) )
        {
            $msjError.= 'Cod usuario Favorito: ' .  $this->msjErrorFormatoCodigo; 
            $error = TRUE;
        }
 
        if($error)
        {
             return $this->retorno(TRUE, 'ERROR, uno o mas campos invalidos. ' . $msjError);
        }
        else
        {
                $data = array('usuario_codigo'=>$this->codigoUsuario,  'usuario_codigo_favorito'=> $codigoFavorito ) ;
                $insert = $this->bd->insert('favorito', $data);
                if($insert === FALSE)
                { 
                    $msjFinal = 'ERROR, NO se pudo registrar el favorito, ya se encuentra registrado para el usuario indicado'; 
                    return $this->retorno(TRUE, $msjFinal);
                }
                else
                {    
                    if($insert == 1)
                    {
                        $msjFinal = 'Favorito creado exitosamente'; 
                        return $this->retorno(FALSE, $msjFinal);
                    }
                    else
                    {
                        $msjFinal = 'ERROR, ya esta definido este favorito para este usuario';
                        return $this->retorno(TRUE, $msjFinal);
                    } 
                } 
        }  
    }


    /* funcion para verificar la existencia de los codigos de usuario en la base de datos
    $codigoUsuario codigo del usuario 
    $codigoFavorito codigo del favorito
    retorna boolean solo si valida ambos TRUE, de lo contrario retorna el codigo del no validado
    */
    private function validaNuevoFavoritoBD($codigoUsuario, $codigoFavorito)
    {
        if(!$this->verificaCodigo('usuario', 'codigo',$codigoUsuario))
        {
            return $codigoUsuario;
        }

        if(!$this->verificaCodigo('usuario', 'codigo',$codigoFavorito))
        {
            return $codigoFavorito;
        }
        return TRUE;
    }

    /*
    set campo codigoUsuario
    $codigo valor a modificar 
    */
	public function setCodigo( $codigo )
	{   
        $this->codigoUsuario = $codigo;
    }

     /*
    get campo codigoUsuario
    devuelve codigo usuario 
    */
	public function getCodigo( )
	{   
        return $this->codigoUsuario;
    }

    /*
    obtiene los favoritos de un usuario
    $codigoUsuario codigo del usuario a obtener
    $texto especifica si retorna en texto o no
    retorna data en texto
    */
	public function getFavoritos($texto = TRUE)
	{  
        if( $this->verificaCodigo('favorito', 'usuario_codigo', $this->codigoUsuario) )
        { 
            $filtro = array("u.usuario_codigo"=> $this->codigoUsuario);
            return $this->lista($texto, $filtro);  
        }
        else
        {
            return $this->retorno(TRUE, 'ERROR, este usuario no posee favoritos');
        } 

    }

    /*
    obtiene los seguidores (los usuarios que lo tienen como favorito)
    $codigoUsuario codigo del usuario a obtener 
    $texto especifica si retorna en texto o no
    retorna data en texto
    */
	public function getSeguidores($texto = TRUE)
	{  

        if( $this->verificaCodigo('favorito', 'usuario_codigo_favorito', $this->codigoUsuario) )
        { 
            $filtro = array("u.usuario_codigo_favorito"=> $this->codigoUsuario);
            return $this->lista($texto, $filtro);  
        }
        else
        {
            return $this->retorno(TRUE, 'ERROR, este usuario no posee seguidores');
        }  
    }
 

    /*
    Eliminar el usuario instanciado o el definido en el parametro
    $codigoUsuario codigo del usuario
    $codigoFavorito codigo del usuario favorito (opcional, solo con el primer parametro se puede eliminar todo sobre el usuario)
    $retornaBand TRUE para indicar retorno en boolean, OPCIONAL
    retorna msj de proceso
    */
    public function delete($codigoFavorito = 0, $retornaBand = FALSE){  
        $condicion =  array('usuario_codigo'=> $this->codigoUsuario, 'usuario_codigo_favorito' => $this->codigoUsuario ); 
        if($codigoFavorito == 0)
        {
            $condicion =  array('usuario_codigo'=> $this->codigoUsuario ); 
        } 
         
        $ejecuto =  $this->ejecutaDelete('favorito', $condicion, ' Favoritos', TRUE);
         
        if($retornaBand)
        {
            return $ejecuto;
        }
        else
        {
            if($ejecuto)
            {
                return $this->retorno(FALSE, $this->msjSuccesEjecucion );
            }
            else
            {
                return $this->retorno(TRUE, $this->msjErrorEjecucion );
            }
        }
         
    } 
     
}

//$bd = new ConnMysql( BD_SERVER, BD_USER, BD_PASS, BD_NAME );  
/*ejemplos de uso, esta clase tiene funcionalidad ideal instanciada desde la clase usuario*/
//$favorito = new Favorito($bd); //se puede instanciar sin usuario en particular  
//echo $favorito->lista(FALSE); // lista todos los favoritos actuales en la tabla
//$favorito->nuevo(4,8); //definir favorito
//echo $favorito->lista(); //listando como texto
//$favorito->delete( 7, 8); // eliminar favorito, el tercer parametro es opcional para retorno en boolean

?>