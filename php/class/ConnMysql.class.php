<?php
/* Creada por jose rivas @tonistation */
/* 16-09-2017 */

/*clase para manejar tareas con bases de datos mysql (plugin mysqli)*/

//incluir archivo clase de log
include('Log.class.php'); 

class ConnMysql
{ 
	//objeto de conexion
	public $connection;
	private $logError;

	public function __construct($host, $usuario, $clave, $db = '')
	{
		$this->logError = new Log();
        $this->connection = new mysqli($host, $usuario, $clave, $db); 
        if(!$this->connection === TRUE)
        {
            $this->logError->log('conexion', $this->connection->error );
        }
	}

    /*
	Ejecutar querys simples que no requieran devolucion de data
	$sql:string consulta
    returna true o false si se logra ejecutar o no
    */
	public function queryExec($sql, $data)
	{    
		if(!empty($sql))
		{
			$result = $this->connection->query($sql);
			if( $result  )
			{
				if($data)
				{ 
					return $result->fetch_all(MYSQLI_ASSOC);
				}
				else{
					return true;
				} 
			}
			else
			{
				$this->logError->log('queryExec', $sql );
				$this->logError->log('queryExec', $this->connection->error );
				return false;
			}
		}
		else
		{
			return false;
		}
		
	}

    /*
	Ejecutar querys que devuelvan data
	$tabla: tabla a consultar
	$camposSelect: campos a obtener
    $camposCondicion: condiciones opcional
    $limit: opcional
    Retorna arreglo asociativo de datos consulta o false si no hay data o hubo error en ejecucion
    */
	public function querySelect($tabla, $camposSelect = "*", $camposCondicion = array(), $limit = "")
	{	 
		$campos;
		if($camposSelect != "*")
		{
			$campos=implode(', ',$camposSelect);
		}
		else
		{
			$campos = $camposSelect;
		}
		

		$i = 0;
		$cond = array();
		foreach($camposCondicion as $indice=>$datoCondicion) 
		{
			$cond[$i] =  $indice." = '".$datoCondicion."'"; 
			$i++;
		}  
		 
        $condicion = implode(' AND ',$cond);

		if(!empty($condicion))
		{
			$condicion = 'WHERE ' . $condicion;
		}
   	
  		$resultado=$this->connection->query('SELECT ' . $campos . ' FROM ' . $tabla .' ' . $condicion);  
  		if($resultado)
  		{ 
  			return $resultado->fetch_all(MYSQLI_ASSOC);
  		}
        else
  		{
  			$this->logError->log('querySelect ' . $tabla, $campos . ' FROM ' . $tabla .' ' . $condicion );
  			$this->logError->log('querySelect ' . $tabla, $this->connection->error );
  			return false;
  		} 
	} 

	/*
	Indica si un valor dado ya se encuentra en la tabla - campo indicado, para evitar duplicados 
	$tabla:string tabla donde se busca
	$campo: campo donde se buscara
    $valor: valor a buscar 
    Retorna true o false si encuentra o no
    */
	public function checkValue($tabla, $campo, $valor)
	{	 
		
		if(empty($tabla) || empty($campo) || empty($valor)){
			$this->logError->log('checkValue ', 'Parametros incorrectos' . $tabla . ' -- ' . $campo . ' -- ' . $valor  );
			return false;
		}
   
  		$resultado=$this->connection->query('SELECT ' . $campo . ' FROM ' . $tabla .' WHERE ' . $campo . ' = "'. $valor . '"');  
  		if($resultado)
  		{ 
			return ($resultado->num_rows > 0);
  		}
        else
  		{
  			$this->logError->log('checkValue ' . $tabla, 'SELECT ' . $campo . ' FROM ' . $tabla .' WHERE ' . $campo . ' = "'. $valor . '"' );
  			$this->logError->log('checkValue ' . $tabla, $this->connection->error );
  			return false;
  		} 
	} 

    /*
	Guardar nuevos datos en la base de datos 
	$tabla:string (nombre de tabla)
	$camposDatos: array (arreglo de campos - datos)
    retorna true si ejecuto o false si hubo algun error
    */
	public function insert($tabla, $camposDatos)
	{

		//separar datos del array
		$campo = implode(', ', array_keys($camposDatos));
		$i = 0;

		foreach($camposDatos as $indice=>$valor)
		{
			$dato[$i] = "'".$valor."'";
			$i++;
		}
 

		$datos = implode(', ',$dato);  

		//ejecutar el insert
		if($this->connection->query('INSERT INTO ' .$tabla. ' ('. $campo .') VALUES (' . $datos .')' ) === TRUE)
		{
			return true;
		}
		else
		{
			$this->logError->log('insert ' . $tabla, $this->connection->error );
			return FALSE;
		}
 

	}

    /*
	Borrar datos de la base de datos
	$tabla:string (nombre de tabla)
	$camposDatos: array (arreglo de campos - datos)
    retorna cantidad de elementos modificados o false por error de ejecucion
    */
	public function delete($tabla, $camposDatos)
	{
		$i = 0;

		//separar datos del array
		foreach($camposDatos as $indice=>$valor) 
		{
			$dato[$i] = $indice." = '".$valor."'";
			$i++;
		} 

		$condicion = implode(' AND ',$dato); 

        //hacer delete
		if($this->connection->query('DELETE FROM ' . $tabla . ' WHERE ' . $condicion ) === TRUE)
		{
            if(mysqli_affected_rows($this->connection))
            { 
                return mysqli_affected_rows($this->connection);
            }  
            else
            {  
                return 0;
            }
        }else{
            $this->logError->log('delete ' . $tabla, $this->connection->error );
            return false;
        }
	}

    /*
	actualizar registros en tablas
	$tabla:string (nombre de tabla)
	$camposSet: array (arreglo de campos - datos para el set) 
	$camposCondicion: array (arreglo de campos - datos condicion)
    retorna cantidad de elementos modificados o false por error de ejecucion
    */
	public function update($tabla, $camposSet, $camposCondicion)
	{
		$i=0;
		foreach($camposSet as $indice=>$dato) 
		{
			$datoSet[$i] = $indice." = '".$dato."'";
			$i++;
		}

		$consultaSet = implode(', ',$datoSet);
		
		$i=0;
		foreach($camposCondicion as $indice=>$datoCondicion) 
		{
			$condicion[$i] = $indice." = '".$datoCondicion."'";
			$i++;
		} 

		$consultaCondicion = implode(' AND ',$condicion);  
        
		if($this->connection->query('UPDATE ' . $tabla . ' SET ' . $consultaSet . ' WHERE ' . $consultaCondicion) === TRUE)
		{
		   	if(mysqli_affected_rows($this->connection))
		   	{ 
		    	return mysqli_affected_rows($this->connection);
		   	}  
		   	else
		   	{  
				return 0;
		  	}
		}else{
            $this->logError->log('update ' . $tabla, $this->connection->error );
            return false;
        }

	} 

	/*
	obtiene ultimo valor de un campo dado
	$tabla: tabla a consultar 
	$campo: campo a buscar OPCIONAL
	$order: campo para ordenar OPCIONAL siempre se ordena descendiente 
    retorna valor o false si no se enceuntra o hay error
    */
	public function ultimoCodigo($tabla, $campo = 'codigo' , $order = 'codigo')
	{   
		if(!empty($tabla))
		{
			$result = $this->connection->query('SELECT ' . $campo . ' FROM ' . $tabla . ' order by ' . $order .' desc limit 0,1');
			if( $result  )
			{
				$data = $result->fetch_all(MYSQLI_ASSOC);
				return $data[0][$campo]; 
			}
			else
			{
				$this->logError->log('ultimoCodigo', $campo . '-' . $order );
				$this->logError->log('ultimoCodigo', $this->connection->error );
				return false;
			}
		}
		else
		{
			return false;
		}
		
	}

}



//***** EJEMPLOS DE USO **////
// $prueba = new ConnMysql('localhost', 'contacto', 'Adm16062010', 'pruebaGeoPagos'); //intancia
 

//ejemplo uso querySelect
/*
$campos = array('usuario', 'edad');
$condicion = array('codigo' => '4');
$dataTodo = $prueba->querySelect('usuario');
$dataFiltro = $prueba->querySelect('usuario', $campos, $condicion);
print_r( $dataTodo );
print_r( $dataFiltro );
*/

//ejemplo uso insert
/*
$usuario = array('usuario'=>'salim',  'clave'=> MD5('verde') , 'edad'=> 80 ) ;
$prueba->insert('usuario', $usuario);
*/


// ejemplo uso update
/* 
$campos =  array('usuario'=>'saul',  'edad'=> 15 ) ;
$condicion =  array('codigo'=> 2 ) ;

$update = $prueba->update('usuario', $campos, $condicion);
if( $update === FALSE ) echo "error de ejecucion";
else echo $update; //imprime cantidad de registros modificados
*/

//ejemplo uso delete
/*
$condicion =  array('codigo'=> 5, 'usuario' => 'salim' ) ;
$delete = $prueba->delete('usuario', $condicion);
if( $delete === FALSE ) echo "error de ejcucion";
else echo $delete; //imprime cantidad de registros eliminados
*/

?>