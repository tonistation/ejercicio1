<?php
/* Creada por jose rivas @tonistation */
/* 16-09-2017 */

/*clase para registro de logs de operaciones*/
class Log
{

	//objeto de conexion
	private $formato;
	private $ruta;

	public function __construct($tipo = "txt")
	{
		$this->formato = $tipo;
		$this->ruta = "../logs/";
	}

	private function getRealIP()
	{ 
	    if (isset($_SERVER["HTTP_CLIENT_IP"]))
	    {
	        return $_SERVER["HTTP_CLIENT_IP"];
	    }
	    elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"]))
	    {
	        return $_SERVER["HTTP_X_FORWARDED_FOR"];
	    }
	    elseif (isset($_SERVER["HTTP_X_FORWARDED"]))
	    {
	        return $_SERVER["HTTP_X_FORWARDED"];
	    }
	    elseif (isset($_SERVER["HTTP_FORWARDED_FOR"]))
	    {
	        return $_SERVER["HTTP_FORWARDED_FOR"];
	    }
	    elseif (isset($_SERVER["HTTP_FORWARDED"]))
	    {
	        return $_SERVER["HTTP_FORWARDED"];
	    }
	    else
	    {
	        return $_SERVER["REMOTE_ADDR"];
	    } 
	} 

	public function log($titulo, $texto)
	{
		$arch = fopen($this->ruta . "log_".date("Y-m-d"). $this->formato, "a+");  
		fwrite($arch, "[".date("Y-m-d H:i:s.u")." ". $this->getRealIP() . "  $titulo ". $texto."\n");
		fclose($arch);
	}
}