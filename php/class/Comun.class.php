<?php

/* Creada por jose rivas @tonistation */
/* 17-09-2017 */

/*clase con funciones referentes al proceso con base de datos y validaciones*/
//se construyo para mantener mas limpio el codigo en las clases relacionadas a las tablas
 
//incluir archivo clase para conectar con bd
include_once('ConnMysql.class.php');
//incluir archivo config para parametros bd
include_once('../config/config.php'); 

class Comun
{ 
    protected $bd;  
     
    protected $msjErrorEjecucion;
    protected $msjSuccesEjecucion;
    
    public $respuesta; 
    //tipo de respuesta en las funciones de ejecucion 'json', 'arrelgo' o 'texto' que es la predeterminada
  
    public function __construct()
	{
        $this->bd = new ConnMysql( BD_SERVER, BD_USER, BD_PASS, BD_NAME );   
        $this->estableceVariables();

    }
    
    private function estableceVariables(){ 
        $this->msjErrorEjecucion = 'ERROR de ejecucion, consulte log.';
        $this->msjSuccesEjecucion = 'Proceso realizado con exito.';  
    }

    /*
    retorna toda la lista de registros en la tabla
    $tabla la tabla a listar
    $enTexto opcional , especifica si retorna la data en texto, si esta en false retorna en arreglo
    $filtro opcional para lista, se recibe array par valor para condicion
    salida arreglo con data o texto plano  
    */
	protected function listaRegistros($tabla, $enTexto = true, $filtro = array())
	{  
        $cabecera = array('usuario' => '<br><b>Codigo || Usuario || Clave || Edad</b><br>',
                          'favorito' => '<br><b>Codigo Usuario || Usuario || Codigo Usuario Fav. || Usuario Favorito </b><br>',
                          'pago' => '<br><b>Codigo Usuario || Usuario || Codigo Pago || Importe || Fecha </b><br>');

      
        $msjFinal = '';
        if(!is_array($filtro))
        {
            return 'ERROR, parametro filtro debe ser un arreglo';
        } 

        $data = array();
        switch ($tabla) {
            case 'pago':  
                $condicion = implode(' ', $filtro);  

                if(!empty(trim($condicion)))
                {
                    $campo = array_keys($filtro); 
                    $condicion = ' WHERE ' . $campo[0] . ' = ' . $condicion;  
                }  
                $data = $this->bd->queryExec($this->sqlJoinPagos . $condicion, TRUE);
                break;
            case 'favorito': 
                $data = $this->bd->queryExec($this->sqlJoinFavoritos, TRUE);
                break;
            default:
                $data = $this->bd->querySelect($tabla, "*", $filtro);
                break;
          }   

        if(count($data) == 0)
        {
            $data = array('error'=>FALSE, 'msj'=>'-- Sin informacion que mostrar (' . $tabla . ') --', 'data' => FALSE);
            return json_encode($data);
        } 

        if(!$enTexto)
        {   
            return $this->retorno(FALSE, 'Correcto', $data); 
        }
        else
        {
            $dataTexto = $cabecera[$tabla];
            foreach($data as $indice) 
            {
                $dataTexto.= implode(' || ', $indice) . '<br>';
            }
            return $dataTexto;
        }   
        
    }
  
     
    /*
    funcion para ejecutar proceso de update y emitir mensajes del resultado
    $tabla tabla donde se realiza el update
    $campos arreglo par campo valor de campos a modificar
    $condicion arreglo par campo valor con la condicion
    */
    protected function ejecutaUpdate($tabla, $campos, $condicion, $infoAdd)
    {
        $msjFinal = '';
        $update = $this->bd->update($tabla, $campos, $condicion);
        
        if( $update === FALSE )
        {
            return $this->retorno(TRUE, $this->msjErrorEjecucion);
        }
        else
        { 
            if($update > 0){ 
                $msjFinal = $this->msjSuccesEjecucion .  'Registro actualizado'; 
                return $this->retorno(FALSE, $this->msjSuccesEjecucion .  ' Registro actualizado (' . $infoAdd . ')');            
            }
            else
            {
                return $this->retorno(FALSE, $this->msjSuccesEjecucion .  ' No se realizaron modificaciones (' . $infoAdd . ')');            
            }
        }

    }

    /*
    funcion para ejecutar el proceso de delete
    $tabla la tabla de la que se eliminara registro
    $condicion la condicion para eliminar, arreglo par campo valor
    $infoAdd informacion adicional para el msj del resultado
    $retornoBand define si retorna boolean o no
    retorna booleano por ejecucion o emite msj de resultado
    */
    protected function ejecutaDelete($tabla, $condicion, $infoAdd, $retornoBand)
    {
        $msjFinal = ''; 
        $error = FALSE;
        $delete = $this->bd->delete($tabla, $condicion);

        if($delete === FALSE)
        {
            $error = TRUE;
            $msjFinal = $this->msjErrorEjecucion;
            
        }
        else
        {
            if($delete > 0){
                $error = FALSE;
                $msjFinal = $this->msjSuccesEjecucion .  'Registro eliminado';
            }
            else
            {
                $error = FALSE;
                $msjFinal = 'No se elimino el registro, revisar parametros de condicion';
            }
        }  

        if($retornoBand)
        { 
            return !$error;
        }
        else
        {  
            return $this->retorno($error, $msjFinal . ' (' . $infoAdd . ')' );
        } 
    }
       

    //funcion para verificar si un codigo de un registro en unra tabla es valido
    //$codigoUsuario es el codigo definido
    protected function verificaCodigo($tabla, $campo, $codigoUsuario)
    {
        if($codigoUsuario != 0)
        {
            if(!is_numeric($codigoUsuario))
            {
               return FALSE;
            }
            else
            { 
                return $this->bd->checkValue($tabla, $campo, $codigoUsuario);                 
            }
        } 
        else{
            return FALSE; 
        } 
    }  

    /*validar usuario*/
    protected function validaUsuario($usuario)  
    { 
        return (preg_match('/^\w{6,15}+$/', $usuario));
    }
    /*validar clave*/
    protected function validaClave($clave)  
    { 
        if(strlen($clave) < 8 || strlen($clave) > 15 || !preg_match('/(?=\d)/', $clave) || !preg_match('/(?=\d)/', $clave) || !preg_match('/(?=[a-zA-Z])/', $clave) )
        {
            return false;
        }
        else
        {
            return true;
        } 
    }
    /*validar edad*/
    protected function validaEdad($edad)  
    { 
        return ( is_numeric($edad) && $edad >= 18);
    }

    /*validar importe*/
    protected function validaImporte($importe)  
    { 
        return (preg_match('/^(?:[0-9]{1,3})(?:,[0-9]{3})*(?:|\.[0-9]+)$/', $importe));
    }
    /*validar fecha*/
    protected function validaFecha($fecha)
    {
        $formato = preg_match('/^([0-2][0-9]|3[0-1])(\/|-)(0[1-9]|1[0-2])\2(\d{4})$/', $fecha);
        if($formato)
        {
            $hoy = strtotime(date('d-m-Y'));
            $fecha = strtotime($fecha);
            return ( $fecha >= $hoy  ); 
        }
        else
        {
            return FALSE;
        }
    }
    /*cambiar formato de fecha para mysql*/
    protected function cambiaFormatoFechaMysql($fecha)
    {  
        return date("Y-m-d", strtotime($fecha));
    }
   
    protected function retorno($error, $msj, $data = array()){
        $respuesta = array('error' => $error, 'msj' => $msj, 'data'=>$data);
          
        switch ($this->respuesta)
        {
            case 'texto':
                $msjError = $error ? '(ERROR)' : '(EXITO)';
                echo '<br><b>' . $msjError . '</b>' . $msj;
                break;
            case 'arreglo':
                return $respuesta;
                break;
            case 'json':
                return json_encode( $respuesta );
                break;
        } 
    }
}
 

?>