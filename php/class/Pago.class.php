<?php

/* Creada por jose rivas @tonistation */
/* 17-09-2017 */

/*clase para el manejo de los pagos*/
 
//incluir archivo clase Proceso
include_once('Comun.class.php');  

class Pago extends Comun
{ 
    protected $bd;  
    private $codigoUsuario; 
    private $codigoPago;
    private $importe;
    private $fecha;
     
    private $msjErrorFormatoImporte;
    private $msjErrorFormatoFecha;  
 
    protected $sqlJoinPagos;

    public function __construct($bd, $codigoUsuario = 0, $codigoPago = 0)
	{ 
        $this->respuesta = 'json';
        $this->bd = $bd;    
        $this->codigoUsuario = $codigoUsuario;
        $this->codigoPago = $codigoPago;
        $this->estableceVariablesPago();
    }
    
    //establece variables que se usan en los mensajes de error
    private function estableceVariablesPago(){
        $this->msjErrorFormatoImporte = ' Importe invalido (Formato: Numerico mayor a cero Ej. 900,000.00)';
        $this->msjErrorFormatoFecha = ' Fecha invalida (Formato: solo se acepta formato DD-MM-YYYY, y que sea mayor al dia actual)'; 
        $this->sqlJoinPagos = 'select `up`.`usuario_codigo` AS `codigoUsuario`,`u`.`usuario` AS `usuario`,`up`.`pago_codigo` AS `codigoPago`,`p`.`importe` AS `importe`,date_format(`p`.`fecha`,\'%d-%m-%Y\') AS `fecha` 
        from ((`usuario_pago` `up` join `usuario` `u` on((`up`.`usuario_codigo` = `u`.`codigo`))) 
        join `pago` `p` on((`up`.`pago_codigo` = `p`.`codigo`)))';
    }

    /*
    retorna todos los pagos actuales
    $enTexto opcional , especifica si retorna la data en texto, si esta en false retorna en arreglo
    $filtro opcional para lista, se recibe array par valor para condicion
    salida arreglo con data 
    */
	public function lista($enTexto = true, $filtro = array())
	{  
         return $this->listaRegistros('pago', $enTexto, $filtro);
    }

     /*
    crea pago  
    retorna mensaje de resultado  
    */
	public function crear()
	{  
        $msjFinal = ''; 
        $valida = $this->validaCampos($this->importe, $this->fecha, $this->codigoUsuario); 
        $msjFinal = $valida['msj'];

        if($valida['error'])
        {
            $msjFinal.= 'Errores encontrados, NO se pudo registrar el favorito Valores proporcionados Fecha: ' . $this->fecha . ' || Importe: ' . $this->importe;
            return $this->retorno(TRUE, $msjFinal);
        }
        else
        { 
                $data = array('importe'=>str_replace(',','',$this->importe),  'fecha'=> $this->cambiaFormatoFechaMysql($this->fecha)) ;
                $insert = $this->bd->insert('pago', $data);
                if($insert === FALSE)
                { 
                    $msjFinal = 'ERROR, NO se pudo registrar el pago '; 
                    return $this->retorno(TRUE, $msjFinal);
                }
                else
                {      
                    $this->setCodigoPago( $this->bd->ultimoCodigo('pago') ); 
                    if($this->registraUsuarioPago())
                    {
                        $msjFinal = 'Pago registrado exitosamente. ';  
                        return $this->retorno(FALSE, $msjFinal);
                    }
                    else
                    {
                        $$this->setCodigoPago(0);
                        $msjFinal = 'ERROR, creando vinculacion de pago con usuario, se revertiran los cambios. ';  
                        $this->delete($ultimoCodigo);
                        return $this->retorno(TRUE, $msjFinal);
                    }
                } 
        }  
    }

     /*
    informacion del pago establecido en la instancia
    retorna informacion de pago o msj de error si no hay definido
    */
	public function getPagoActual()
	{  
        $data = ''; 
        if( $this->verificaCodigo('pago', 'codigo', $this->codigoPago) )
        {
            $data = $this->lista(TRUE, array("codigo"=> $this->codigoPago)); 
            return $this->retorno(FALSE, 'Correcto', $data);
        }
        else
        {
            return $this->retorno(TRUE, 'ERROR, codigo de pago no existe en la base de datos');
        } 
    }

    /*
    informacion de los pagos del usuario establecido
    $texto indica si responde en texto o no
    retorna informacion de pago o msj de error si no hay definido
    */
	public function getPagosUsuarioActual($texto = TRUE)
	{  
        $data = ''; 
        if( $this->verificaCodigo('usuario', 'codigo', $this->codigoUsuario) )
        { 
            $filtro = array('up.usuario_codigo' => $this->codigoUsuario );
            return $this->lista($texto, $filtro);  
            //return $this->retorno(FALSE, 'Correcto', $data);
        }
        else
        {
            return $this->retorno(TRUE, 'ERROR, codigo de usuario no existe en la base de datos');
        } 
    }
 
    /*
    Eliminar el pago instanciado o el definido en el parametro 
    retorna msj de proceso
    */
    public function delete(){  
        $condicion =  array('codigo'=> $this->codigoPago );   
        $msjFinal = '';

        if($this->verificaCodigo('pago', 'codigo', $this->codigoPago ))
        {
            if($this->deleteUsuarioPago($this->codigoPago) )
            {
                 $this->ejecutaDelete('pago', $condicion, ' Pago', FALSE);
                 return $this->retorno(FALSE, 'Pago eliminado correctamente');
            }
            else
            {
                $msjFinal = 'ERROR, en el proceso eliminando las vinculaciondes de pagos con usuarios (Eliminar Pago)';
                return $this->retorno(TRUE, $msjFinal);
            }
        }
        else{
            $msjFinal = 'ERROR, codigo de pago invalido (Eliminar Pago)';
            return $this->retorno(TRUE, $msjFinal);
        }  
    }

     /*
    Funcion para eliminar vinculacion de pago de un usario (aplica al eliminar pago o usuario) 
    $codigoPago, codigo del pago  OPCIONAL
    retorna msj de proceso
    */
    public function deleteUsuarioPago(){ 
        $condicion =  array('pago_codigo' => $this->codigoPago );   
        return $this->ejecutaDelete('usuario_pago', $condicion, '', TRUE);
    } 


     /*
    Funcion para eliminar vinculacion de pago de un usario por el codigo de usuario
    $codigoUsuario codigo del usuario  
    retorna msj de proceso
    */
    public function deletePorUsuario(){  
        if($this->verificaCodigo('usuario', 'codigo', $this->codigoUsuario))
        {
            $filtro = array('usuario_codigo' => $this->codigoUsuario);
            $data = $this->bd->querySelect('usuario_pago', array('pago_codigo'), $filtro);
            $nPagos = 0;
            foreach($data as $indice) 
            {
                 $this->delete($indice['pago_codigo']);
                 $nPagos++;
            }
            $condicion =  array('usuario_codigo' => $this->codigoUsuario );   
            $this->ejecutaDelete('usuario_pago', $condicion, '', TRUE);
            return $this->retorno(FALSE, 'Correcto, ' . $nPagos . ' eliminados.');
        }
        else
        { 
            return $this->retorno(TRUE, 'ERROR, no se pueden eliminar los pagos asociados al usuario: ' . $this->codigoUsuario); 
        } 
    }  

    /*
    Funcion para crear vinculacion usuario y pago 
    */
    private function registraUsuarioPago(){   
        $data = array('usuario_codigo'=>$this->codigoUsuario,  'pago_codigo'=> $this->codigoPago ) ;
        $insert = $this->bd->insert('usuario_pago', $data);
        if($insert === FALSE)
        { 
            $msjFinal = 'ERROR, NO se pudo registrar el pago. Problema con la vinculacion al usuario '; 
            return FALSE;
        }
        else
        {     
            return TRUE;
        } 
    }
 

    /* ejerce tareas de validacion comunes*/
    function validaCampos()
    {   
        $informe = array('error' => FALSE, 'msj' => '');
        if(!$this->verificaCodigo('usuario', 'codigo', $this->codigoUsuario))
        {
            $informe['error'] = TRUE;
            $informe['msj'] = 'ERROR, usuario invalido'; 
        }

        if(!$this->validaFecha($this->fecha))
        {   
            $informe['error'] = TRUE;
            $informe['msj'].= $this->msjErrorFormatoFecha; 
        }
        if(!$this->validaImporte($this->importe) || !($this->importe > 0.00 ) )
        {   
            $informe['error'] = TRUE;
            $informe['msj'].= ' ' . $this->msjErrorFormatoImporte; 
        }  

        return $informe;
    }
 

    /*    set campo codigoUsuario
    $codigo valor a modificar     */
	public function setCodigoUsuario( $codigo )
	{   
        $this->codigoUsuario = $codigo;
    }

    /*  set campo codigoPago
    $codigo valor a modificar     */
	public function setCodigoPago( $codigo )
	{   
        $this->codigoPago = $codigo;
    }

    /*    set campo importe
    $codigo valor a modificar     */
	public function setImporte( $importe )
	{   
        $this->importe = $importe;
    }

    /*   set campo fecha    
    $codigo valor a modificar     */
	public function setFecha( $fecha )
	{   
        $this->fecha = $fecha;
    }

     /* get campo codigoUsuario   */
	public function getCodigoUsuario( )
	{   
        return $this->codigoUsuario;
    } 

     /*    get campo codigoPago    */
	public function getCodigoPago( )
	{   
        return $this->codigoPago;
    } 

     /*    get campo importe    */
	public function getImporte( )
	{   
        return $this->importe;
    } 

     /*    get campo fecha    */
	public function getFecha( )
	{   
        return $this->fecha;
    } 


     
}

//$bd = new ConnMysql( BD_SERVER, BD_USER, BD_PASS, BD_NAME ); 
//$pago = new Pago($bd); //primer parametro bd, segundo OPCIONAL codido de usuario, tercer OPCIONAL codigo pago
//echo $pago->lista(FALSE); //listar todos los pagos
//$pago->setCodigoUsuario(9);
//$pago->setFecha('22-12-2017');
//$pago->setImporte(145);
//echo $pago->crear();

//$pago->setCodigoPago(9); //asignar codigo de pago para trabajar con uno en particular
//echo $pago->getPagosUsuarioActual(FALSE);

//echo $pago->delete(); // elimina el pago nro 7 asi como sus vinculaciones
//echo $pago->lista();

?>