<?php
/*para respuesta en formato json*/
header('Content-Type: application/json');

//archivo de conexion a bd
require_once('../class/Usuario.class.php');
//archivo de configuracion de conexion a bd
require_once('../config/config.php');

//archivo clase usuario
require_once('../class/Usuario.class.php');

$accion = isset($_GET['a']) ? $_GET['a'] : ''; 
if(empty($accion))
{
    die('ERROR EN DATOS DE ENTRADA');
}

//error_reporting(0);

$dataPost = false;  
 
 function ejecucion($accion)
{
    $bd = new ConnMysql( BD_SERVER, BD_USER, BD_PASS, BD_NAME );
    $usuario = new Usuario($bd);  
    $usuario->respuesta='json';

    switch($accion)
    {
        case 'usuario_get':  
            listaUsuarios($usuario);
            break;
        case 'usuario_post':  
            creaUsuario($usuario);
            break; 
        case 'usuario_put':  
            editaUsuario($usuario);
            break;
        case 'usuario_delete':  
            eliminaUsuario($usuario);
            break;  
        case 'usuario_pagos':  
            listaPagos($usuario);
            break;
        case 'usuario_pago_post':  
            $pago = new Pago($bd);  
            registraPago($pago);
            break;
        case 'usuario_pago_delete':  
            $pago = new Pago($bd);  
            eliminaPago($pago);
            break;
        case 'usuario_favoritos':  
            listaFavoritos($usuario);
            break;
        case 'usuario_favorito_post':  
            registraFavorito($usuario);
            break;
        case 'usuario_favorito_delete':  
            eliminaFavoritos($usuario);
            break;
        case 'usuario_seguidores':  
            listaSeguidores($usuario);
            break;
        case 'pago_get': 
            $pago = new Pago($bd); 
            listaPagosAll($pago);
            break; 
        default:
            retorno(TRUE, ' Error en solicitud, verifique parametros accion=' . $accion);
    }
} 
 

/*listar usuarios*/
function listaUsuarios($usuario)
{   
    $usuario->respuesta='json'; 
    echo $usuario->lista(FALSE); 
}

/*crear usuarios*/
function creaUsuario($usuario)
{   
    $data = getPost();
    if($data)
    {
        $usuario->setUsuario($data->usuario);
        $usuario->setEdad($data->edad);
        $usuario->setClave($data->clave);
        echo $usuario->crear();
    } 
    else
    {
        retorno(TRUE, ' Error en solicitud, verifique parametros');
    }
}

/*edita usuarios*/
function editaUsuario($usuario)
{   
    $data = getPost();
    if($data)
    {
        $usuario->setCodigo($data->codigo);
        $usuario->setUsuario($data->usuario); 
        if(!empty($data->cambiaClave))
        {
            $usuario->setClave($data->clave);
        }
        else
        {
            $usuario->setClave(NULL);
        }
        $usuario->setEdad($data->edad); 
        echo $usuario->actualizar();
    } 
    else
    {
        retorno(TRUE, ' Error en solicitud, verifique parametros');
    }
}
/*elimina usuario*/
function eliminaUsuario($usuario)
{   
    $data = getPost();
    if($data)
    {
        $usuario->setCodigo($data->codigo); 
        echo $usuario->eliminar();
    } 
    else
    {
        retorno(TRUE, ' Error en solicitud, verifique parametros');
    }
}

/*lista pagos*/ 
function listaPagos($usuario)
{   
    if(isset($_GET['codigo']))
    {
        $usuario->setCodigo($_GET['codigo']); 
        echo $usuario->pagos(FALSE);
    } 
    else
    {
        retorno(TRUE, ' Error en solicitud, verifique parametros');
    }
}

/*registra pagos*/
function registraPago($pago)
{   
    $data = getPost();
    if($data)
    {
        $pago->setCodigoUsuario($data->codigoUsuario); 
        $pago->setImporte($data->importe);
        $pago->setFecha($data->fecha); 
        echo $pago->crear();
    } 
    else
    {
        retorno(TRUE, ' Error en solicitud, verifique parametros');
    }
}
/*elimina pagos*/
function eliminaPago($pago)
{   
    $data = getPost();
    if($data)
    {
        $pago->setCodigoPago($data->codigo);  
        echo $pago->delete();
    } 
    else
    {
        retorno(TRUE, ' Error en solicitud, verifique parametros');
    }
} 

/*lista favorito*/ 
function listaFavoritos($usuario)
{   
    if(isset($_GET['codigo']))
    {
        $usuario->setCodigo($_GET['codigo']); 
        echo $usuario->favoritos(FALSE);
    } 
    else
    {
        retorno(TRUE, ' Error en solicitud, verifique parametros');
    }
}

/*registra favoritos*/
function registraFavorito($usuario)
{   
    $data = getPost();
    if($data)
    {
        $usuario->setCodigo($data->codigo);
        echo $usuario->nuevoFavorito($data->codigoFavorito);  
    } 
    else
    {
        retorno(TRUE, ' Error en solicitud, verifique parametros');
    }
}
/*elimina favoritos*/
function eliminaFavoritos($usuario)
{   
    $data = getPost();
    if($data)
    {
        $usuario->setCodigo($data->codigo);
        echo $usuario->deleteFavorito($data->codigoFavorito);  
    } 
    else
    {
        retorno(TRUE, ' Error en solicitud, verifique parametros');
    }
} 

/*obtiene data de entrada*/
function getPost(){
    if(isset($_POST['data']) )
    {
        return json_decode($_POST['data']);
    }
    else
    {
        return false;
    } 
}  

/*lista seguidores*/ 
function listaSeguidores($usuario)
{   
    if(isset($_GET['codigo']))
    {
        $usuario->setCodigo($_GET['codigo']); 
        echo $usuario->seguidores(FALSE);
    } 
    else
    {
        retorno(TRUE, ' Error en solicitud, verifique parametros');
    }
}


/*listar todos los pagos de la tabla*/
function listaPagosAll($pago)
{   
    $pago->respuesta='json'; 
    echo $pago->lista(FALSE); 
}


function retorno($error, $msj)
{
    echo json_encode(array('error'=>$error, 'msj'=>$msj));
    return true;
}  
 

ejecucion($accion);

?>