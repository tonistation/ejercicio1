app.controller('pagoCtrl', pagoCtrl); 

function pagoCtrl($scope, $rootScope, $http, API, $mdDialog){  
  $rootScope.titulo = "Ejercicio 1 - PAGOS"; 

  $scope.pagos = {};
  $scope.pagosSearch = '';

  //para seleccionar usuarios
  $scope.usuarios = {};

  $scope.hayData = false;
  $scope.buscando = false;
  $scope.sinData = false;

  //modal registro pago
  $scope.nuevoPagoA = {};
  $scope.registrandoModal = false;
  
  //variables de error modal
  $scope.muestraErrorModal = false;
  $scope.errorModal = "";

  $scope.cargaPagos = function(){
    $scope.usuarioSearch = '';
    $http.get( API + 'pago_get' ).then( function(respuesta){ 
      if(respuesta.data.error == false)  
      {
        $scope.pagos = respuesta.data.data;    
      }
      else
      {  
        $scope.pagos = {};
        $scope.sinData = true;
      }
    }) 
  }

  $scope.cargaPagos();

  /*modal data pago se usa para crear y editar
  tipo 1 si es nuevo 2 par modificar
  codigo del pago aplica si es modificar
  */
  $scope.modalAddPago = function(tipo, usuario) 
  { 
     $scope.cargaUsuarios();
     $scope.nuevoPago = {};
     $scope.muestraErrorModal = false;
     $scope.errorModal = ""; 
 

     var parentEl = angular.element(document.body); 
     $mdDialog.show({
       parent: parentEl,
       templateUrl: 'app/modals/addPago.html',
       scope:$scope,
       preserveScope: true,
       clickOutsideToClose: true,
       openFrom : "#addPago",
       closeTo : "#addPago", 
       hasBackdrop: true,  
       controller: pagoCtrl
    });
  }
    
  $scope.crear = function(){    
      $scope.registrandoModal = true;    

      $http({
        url: API + "usuario_pago_post",
        method: "POST", 
        data: 'data='+JSON.stringify( $scope.nuevoPago )
      })
      .then(function(response) {
        $scope.registrandoModal = false;
        if(response.data.error == false){ 
            $scope.cargaPagos();
            $scope.nuevoPago = {};
            $mdDialog.hide(); 
        }else{
            $scope.muestraErrorModal = true;
            $scope.errorModal = response.data.msj;
        }

      }, 
      function(response) { // optional
        $scope.registrandoModal = true;
      });  
    }  
 
  
  $scope.cierraModal = function(){
    $mdDialog.hide(); 
  }

  //ventana de confirmacion antres de eliminar
  //texto del msj, codigo del elemento a eliminar y la tabla:
  $scope.confirmaEliminar = function(texto, codigoEliminar) 
  {
    // Appending dialog to document.body to cover sidenav in docs app
    var confirm = $mdDialog.confirm()
          .title('Confirmar accion')
          .textContent(texto)  
          .ok('Si, eliminar')
          .cancel('Cancelar');

    $mdDialog.show(confirm).then(function() { 
        $scope.eliminar(codigoEliminar); 
    }); 
  };

  $scope.eliminar = function(codigoEliminar)
  {
      $http({
        url: API + "usuario_pago_delete",
        method: "POST", 
        data: 'data='+JSON.stringify( { codigo: codigoEliminar } )
      })
      .then(function(response) { 
          if(response.data.error == false)
          {
            $scope.cargaPagos();
          }
          $scope.alertPretty(response.data.msj, function(){});  
      }, 
      function(response) { // optional
        $scope.registrandoModal = true;
      });  
  }

  $scope.alertPretty = function(texto, despues) {
    $mdDialog.show(
      $mdDialog.alert()
        .parent(angular.element(document.querySelector('body')))
        .clickOutsideToClose(true)
        .title('Informacion')
        .textContent(texto) 
        .ok('Aceptar')
    ).then(function() { 
      despues();
    })
  }; 

  /*casrgar usuarios, se usa para registrar pago*/
  $scope.cargaUsuarios = function(){
    $scope.usuarioSearch = '';
    $http.get( API + 'usuario_get' ).then( function(respuesta){ 
      if(respuesta.data.error == false)  
      {
        $scope.usuarios = respuesta.data.data;    
      }
      else
      {  
        $scope.usuarios = {};
        $scope.sinData = true;
      }
    }) 
  }
  
}  