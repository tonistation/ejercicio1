//Controlador para manejar operaciones de usuario
app.controller('usuarioCtrl', usuarioCtrl);
function usuarioCtrl($scope, $rootScope, $http, API, $mdDialog ){  
  $rootScope.titulo = "Ejercicio 1 - USUARIOS"; 

  $scope.usuarios = {};
  $scope.usuarioSearch = '';

  $scope.hayData = false;
  $scope.buscando = false;
  $scope.sinData = false;

  //modal registro usuario 
  $scope.nuevoUsuario = {};
  $scope.registrandoModal = false;

  //arreglo para nuevo pago
  $scope.nuevoPago = {}; 
  //lista de pagos del usuario
  $scope.listPagos = {}; 

  $scope.nuevoFavorito = {}; 
  $scope.listFavoritos = {}; 


  $scope.listSeguidores = {}; 

  //variables de error modal
  $scope.muestraErrorModal = false;
  $scope.errorModal = "";

  $scope.cargaUsuarios = function(){
    $scope.usuarioSearch = '';
    $http.get( API + 'usuario_get' ).then( function(respuesta){ 
      if(respuesta.data.error == false)  
      {
        $scope.usuarios = respuesta.data.data;    
      }
      else
      {  
        $scope.usuarios = {};
        $scope.sinData = true;
      }
    }) 
  }
 	  
  $scope.cargaUsuarios();

  /*modal data usuario se usa para crear y editar
  tipo 1 si es nuevo 2 par modificar
  codigo del usuario aplica si es modificar
  */
  $scope.modalAddUsuario = function(tipo, usuario) 
  { 
     $scope.nuevoUsuario = {};
     $scope.muestraErrorModal = false;
     $scope.errorModal = "";
     $scope.nuevoUsuario.cambiaClave = true; 

     if(tipo == 2)
     {  
       $scope.nuevoUsuario.codigo = usuario.codigo;
       $scope.nuevoUsuario.usuario = usuario.usuario;
       $scope.nuevoUsuario.edad = usuario.edad;
       $scope.nuevoUsuario.clave = '';  
       $scope.nuevoUsuario.clave2 = ''; 
       $scope.nuevoUsuario.cambiaClave = false; 
     }

     var parentEl = angular.element(document.body); 
     $mdDialog.show({
       parent: parentEl,
       templateUrl: 'app/modals/addUsuario.html',
       scope:$scope,
       preserveScope: true,
       clickOutsideToClose: true,
       openFrom : "#addUsuario",
       closeTo : "#addUsuario", 
       hasBackdrop: true,  
       controller: $scope => $scope.nuevoUsuario = $scope.nuevoUsuario
    });
  }
  
  $scope.determinaAccion = function(cambiaClave)
  {
    if($scope.nuevoUsuario.codigo > 0)
    {
      $scope.nuevoUsuario.cambiaClave = cambiaClave;
      $scope.editar();
    }
    else
    {
      $scope.crear();
    }
  }
 

  $scope.crear = function(){  
    if($scope.nuevoUsuario.clave == $scope.nuevoUsuario.clave2)
    { 
      $scope.registrandoModal = true; 
      $http({
        url: API + "usuario_post",
        method: "POST", 
        data: 'data='+JSON.stringify($scope.nuevoUsuario)
      })
      .then(function(response) {
        $scope.registrandoModal = false;
        if(response.data.error == false){ 
            $scope.cargaUsuarios();
            $scope.nuevoUsuario = {};
            $mdDialog.hide(); 
        }else{
            $scope.muestraErrorModal = true;
            $scope.errorModal = response.data.msj;
        }

      }, 
      function(response) { // optional
        $scope.registrandoModal = true;
      });  
    }
    else
    { 
      $scope.muestraErrorModal = true;
      $scope.errorModal = 'La clave y el campo confirmacion de clave deben ser iguales';
    } 
  } 

  $scope.editar = function(){  
    if($scope.nuevoUsuario.clave == $scope.nuevoUsuario.clave2)
    { 
      $scope.registrandoModal = true; 
      $http({
        url: API + "usuario_put",
        method: "POST", 
        data: 'data='+JSON.stringify($scope.nuevoUsuario)
      })
      .then(function(response) {
        $scope.registrandoModal = false;
        if(response.data.error == false){ 
            $scope.cargaUsuarios();
            $scope.nuevoUsuario = {};
            $mdDialog.hide(); 
            $scope.muestraErrorModal = False;
        }else{
            $scope.muestraErrorModal = true;
            $scope.errorModal = response.data.msj;
        }

      }, 
      function(response) { // optional
        $scope.registrandoModal = true;
      });  
    }
    else
    { 
      $scope.muestraErrorModal = true;
      $scope.errorModal = 'La clave y el campo confirmacion de clave deben ser iguales';
    } 
  } 
  
  $scope.cierraModal = function(){
    $mdDialog.hide(); 
  }

  //ventana de confirmacion antres de eliminar
  //texto del msj, codigo del elemento a eliminar y la tabla: 1 usuario, 2 pago, 3 favorito
  $scope.confirmaEliminar = function(texto, codigoEliminar, tabla) 
  {
    // Appending dialog to document.body to cover sidenav in docs app
    var confirm = $mdDialog.confirm()
          .title('Confirmar accion')
          .textContent(texto)  
          .ok('Si, eliminar')
          .cancel('Cancelar');

    $mdDialog.show(confirm).then(function() {
      if(tabla == 1)
      {
        $scope.eliminar(codigoEliminar);
      }
      else if(tabla == 2)
      { 
        $scope.eliminarPago(codigoEliminar);
      }
      
    }); 
  };

  $scope.eliminar = function(codigoEliminar)
  {
      $http({
        url: API + "usuario_delete",
        method: "POST", 
        data: 'data='+JSON.stringify( { codigo: codigoEliminar } )
      })
      .then(function(response) { 
          if(response.data.error == false)
          {
            $scope.cargaUsuarios();
          }
          $scope.alertPretty(response.data.msj, function(){});  
      }, 
      function(response) { // optional
        $scope.registrandoModal = true;
      });  
  }

  $scope.alertPretty = function(texto, despues) {
    $mdDialog.show(
      $mdDialog.alert()
        .parent(angular.element(document.querySelector('body')))
        .clickOutsideToClose(true)
        .title('Informacion')
        .textContent(texto) 
        .ok('Aceptar')
    ).then(function() { 
      despues();
    })
  };


  /**** pagoosss  */

  $scope.cargaPagos = function(codigo){ 
    $scope.codigoTemp = codigo;
    $http.get( API + 'usuario_pagos&codigo='+codigo ).then( function(respuesta){ 
      if(respuesta.data.error == false) 
      {
        $scope.listPagos = respuesta.data.data;   
      } 
    }) 
  }

  $scope.modalPagos = function(codigo){
    $scope.cargaPagos(codigo);
    $scope.formAgregaPago = false;

    var parentEl = angular.element(document.body);
    $mdDialog.show({
       parent: parentEl,
       templateUrl: 'app/modals/usuarioPagos.html',
       scope:$scope,
       preserveScope: true,
       clickOutsideToClose: true, 
       hasBackdrop: true,  
      controller: $scope => $scope.listPagos = $scope.listPagos
    });
  }

  $scope.registraPago = function(){ 
    $scope.nuevoPago.codigoUsuario = $scope.codigoTemp;
    $http({
      url: API + "usuario_pago_post",
      method: "POST", 
      data: 'data='+JSON.stringify($scope.nuevoPago)
    })
      .then(function(response) {
        $scope.registrandoModal = false; 
        if(response.data.error == false){ 
            $scope.cargaPagos($scope.codigoTemp);
            $scope.nuevoPago = {}; 
            $scope.muestraErrorModal = false;
        }else{
            $scope.muestraErrorModal = true;
            $scope.errorModal = response.data.msj;
        }

      }, 
      function(response) { // optional
        $scope.registrandoModal = true;
      });  
  } 


  $scope.eliminarPago = function(codigoEliminar)
  {
      $http({
        url: API + "usuario_pago_delete",
        method: "POST", 
        data: 'data='+JSON.stringify( { codigo: codigoEliminar } )
      })
      .then(function(response) { 
          if(response.data.error == false) 
          $scope.alertPretty(response.data.msj, function(){
            $scope.modalPagos($scope.codigoTemp);
          })  
      }, 
      function(response) { // optional
        $scope.registrandoModal = true;
      });  
  }


  /**** favoritos  */ 
  $scope.cargaFavoritos = function(codigo){ 
    $scope.codigoTemp = codigo; 
    $http.get( API + 'usuario_favoritos&codigo='+codigo ).then( function(respuesta){ 
      if(respuesta.data.error == false) 
      {
        $scope.listFavoritos = respuesta.data.data;   
      }
      else
      {
        $scope.listFavoritos = {};
      }
    }) 
  }

  $scope.modalFavoritos = function(codigo){
    $scope.listFavoritos = {};
    $scope.cargaFavoritos(codigo);
    $scope.formAgregaFav = false;

    var parentEl = angular.element(document.body);
    $mdDialog.show({
       parent: parentEl,
       templateUrl: 'app/modals/usuarioFavoritos.html',
       scope:$scope,
       preserveScope: true,
       clickOutsideToClose: true, 
       hasBackdrop: true,  
       controller: $scope => $scope.listFavoritos = $scope.listFavoritos
    });
  }

  $scope.registraFavorito = function(){ 
    $scope.nuevoFavorito.codigo = $scope.codigoTemp;
    $scope.nuevoFavorito.codigoFavorito = $scope.selectU;
    $http({
      url: API + "usuario_favorito_post",
      method: "POST", 
      data: 'data='+JSON.stringify($scope.nuevoFavorito)
    })
      .then(function(response) {
        $scope.registrandoModal = false; 
        if(response.data.error == false){ 
            $scope.cargaFavoritos($scope.codigoTemp);
            $scope.nuevoFavorito = {}; 
            $scope.muestraErrorModal = false;
        }else{ 
            $scope.muestraErrorModal = true;
            $scope.errorModal = response.data.msj;
        }

      }, 
      function(response) { // optional
        $scope.registrandoModal = true;
      });  
  } 


  $scope.eliminarFavorito = function(codigoEliminar)
  {
      $http({
        url: API + "usuario_favorito_delete",
        method: "POST", 
        data: 'data='+JSON.stringify( { codigo: $scope.codigoTemp, codigoFavorito: codigoEliminar } )
      })
      .then(function(response) { 
          if(response.data.error == false) 
          $scope.alertPretty(response.data.msj, function(){
            $scope.modalFavoritos($scope.codigoTemp);
          })  
      }, 
      function(response) { // optional
        $scope.registrandoModal = true;
      });  
  }

  /**** seguidores  */ 
  $scope.cargaSeguidores = function(codigo){ 
    $scope.codigoTemp = codigo; 
    $http.get( API + 'usuario_seguidores&codigo='+codigo ).then( function(respuesta){ 
      if(respuesta.data.error == false) 
      {
        $scope.listSeguidores = respuesta.data.data; 
      }
      else
      {
        $scope.listSeguidores = {};
      }
    }) 
  } 

  /* modal seguidores */
  $scope.modalSeguidores = function(codigo){
    $scope.cargaSeguidores(codigo);  
    var parentEl = angular.element(document.body);
    $mdDialog.show({
      parent: parentEl,
      templateUrl: 'app/modals/usuarioSeguidores.html',
      scope:$scope,
      preserveScope: true,
      clickOutsideToClose: true, 
      hasBackdrop: true,  
      controller: $scope => $scope.listSeguidores = $scope.listSeguidores
    });
  }
  
}  