app.controller('mlateralCtrl', mlateralCtrl); 
function mlateralCtrl($scope, $rootScope, $mdSidenav, $mdMedia, $mdPanel, $http, API, $location){  

	$scope.tituloListMenu = "Secciones";
	$scope.listMenu = [
		{ 	id: 1, icono :"home", titulo: "Inicio", descripcion: "Inicio", ruta: "" },
	 	{ 	id: 2, icono :"person", titulo: "Usuarios", descripcion: "Administrar usuarios", ruta: "#!/usuarios" },
	 	{	id: 3, icono :"attach_money", titulo: "Pagos", descripcion: "Administrar Pagos", ruta: "#!/pagos"	} , 
	 	{	id: 4, icono :"info", titulo: "Informacion", descripcion: "", ruta: "#!/info"	}  
	];

	$scope.tituloListConf = "Configuracion";
	$scope.listConf = [
	 	{ 	id: 1, icono : "person_pin", titulo: "Mi Perfil Linkedin", accion: "perfil" }, 
	 	{	id: 2, icono : "power_settings_new", titulo: "Salir", accion: "salir"}
	];

	$scope.contenidoActivo = function(contenidoActual){ 
		return contenidoActual == "#" + $location.path();
	} 

	$scope.ajustaLat = function(accion){ 
		if(accion == "perfil"){
			$rootScope.irA("https://www.linkedin.com/in/tonistation/");
		}
		if(!$rootScope.esDesktop){
			$mdSidenav('left').toggle();
		} 
	}
  
}  