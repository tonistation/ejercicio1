angular.module('frontGeoPagos').service('funciones', function ($location, $rootScope,$http, API) {  

    this.notif = function(texto, icono, cod_error){
    	 $mdToast.show(
    	 	$mdToast.simple().textContent(texto).hideDelay(8000).toastClass('notif').position('right','top')
    	 );
    }

    this.esEntero = function(numero){
        if (isNaN(numero)){
            return false;
        }
        else{
            if (numero % 1 == 0) {
                return true;
            }
            else{
                return false;
            }
        }
    } 

    this.irA = function(){
         
    }
 
  })